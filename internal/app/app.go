package app

import (
	"log/slog"
	"time"

	grpcapp "gitlab.com/Alexandrhub/grpc-auth-urlshortener/internal/app/grpc"
	"gitlab.com/Alexandrhub/grpc-auth-urlshortener/internal/services/auth"
	"gitlab.com/Alexandrhub/grpc-auth-urlshortener/internal/storage/sqlite"
)

type App struct {
	GRPCServer *grpcapp.App
}

func New(log *slog.Logger, grpcPort int, storagePath string, tokenTTL time.Duration) *App {
	storage, err := sqlite.New(storagePath)
	if err != nil {
		panic(err)
	}

	authService := auth.New(log, storage, storage, storage, tokenTTL)

	grpcApp := grpcapp.New(log, authService, grpcPort)

	return &App{
		GRPCServer: grpcApp,
	}
}
