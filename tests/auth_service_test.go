package tests

import (
	"testing"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	ssov1 "gitlab.com/Alexandrhub/grpc-auth-urlshortener/gen/go/sso"
	"gitlab.com/Alexandrhub/grpc-auth-urlshortener/tests/suite"
)

const (
	emptyAppID = 0
	appID      = 1
	appSecret  = "test-secret"
	wrongAppId = 2

	passDefaultLen = 10
)

func randomFakePassword() string {
	return gofakeit.Password(true, true, true, true, false, passDefaultLen)
}

func TestRegisterLogin_Login_HappyPath(t *testing.T) {
	ctx, st := suite.New(t)

	email := gofakeit.Email()
	pass := randomFakePassword()

	respReg, err := st.AuthClient.Register(
		ctx, &ssov1.RegisterRequest{
			Email:    email,
			Password: pass,
		},
	)
	require.NoError(t, err)
	assert.NotEmpty(t, respReg.GetUserId())

	respLogin, err := st.AuthClient.Login(
		ctx, &ssov1.LoginRequest{
			Email:    email,
			Password: pass,
			AppId:    appID,
		},
	)
	require.NoError(t, err)
	token := respLogin.GetToken()
	assert.NotEmpty(t, token)

	loginTime := time.Now()

	tokenParsed, err := jwt.Parse(
		token, func(token *jwt.Token) (interface{}, error) {
			return []byte(appSecret), nil
		},
	)
	require.NoError(t, err)

	claims, ok := tokenParsed.Claims.(jwt.MapClaims)
	require.True(t, ok)

	assert.Equal(t, respReg.GetUserId(), int64(claims["uid"].(float64)))
	assert.Equal(t, email, claims["email"].(string))
	assert.Equal(t, appID, int(claims["app_id"].(float64)))

	const deltaSeconds = 1

	// check if exp of token is in correct range, ttl gets from cfg
	assert.InDelta(t, loginTime.Add(st.Cfg.TokenTTL).Unix(), claims["exp"].(float64), deltaSeconds)
}

func TestRegisterLogin_DuplicatedRegistration(t *testing.T) {
	ctx, st := suite.New(t)

	email := gofakeit.Email()
	pass := randomFakePassword()

	respReg, err := st.AuthClient.Register(
		ctx, &ssov1.RegisterRequest{
			Email:    email,
			Password: pass,
		},
	)
	require.NoError(t, err)
	assert.NotEmpty(t, respReg.GetUserId())

	respReg, err = st.AuthClient.Register(
		ctx, &ssov1.RegisterRequest{
			Email:    email,
			Password: pass,
		},
	)
	require.Error(t, err)
	assert.Empty(t, respReg.GetUserId())
	assert.ErrorContains(t, err, "user already exists")
}

func TestRegister_FailCases(t *testing.T) {
	ctx, st := suite.New(t)

	testCases := []struct {
		name        string
		email       string
		password    string
		expectedErr string
	}{
		{
			name:        "empty email",
			email:       "",
			password:    randomFakePassword(),
			expectedErr: "empty email",
		},
		{
			name:        "empty password",
			email:       gofakeit.Email(),
			password:    "",
			expectedErr: "empty password",
		},
		{
			name:        "empty pass and email",
			email:       "",
			password:    "",
			expectedErr: "empty email",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(
			tc.name, func(t *testing.T) {
				t.Parallel()

				_, err := st.AuthClient.Register(
					ctx, &ssov1.RegisterRequest{
						Email:    tc.email,
						Password: tc.password,
					},
				)
				require.Error(t, err)
				assert.ErrorContains(t, err, tc.expectedErr)
			},
		)
	}
}

func TestLogin_FailCases(t *testing.T) {
	ctx, st := suite.New(t)

	testCases := []struct {
		name        string
		email       string
		password    string
		appID       int32
		expectedErr string
	}{
		{
			name:        "Login with empty email",
			email:       "",
			password:    randomFakePassword(),
			appID:       appID,
			expectedErr: "empty email",
		},
		{
			name:        "Login with empty password",
			email:       gofakeit.Email(),
			password:    "",
			appID:       appID,
			expectedErr: "empty password",
		},
		{
			name:        "Login with empty email and password",
			email:       "",
			password:    "",
			appID:       appID,
			expectedErr: "empty email",
		},
		{
			name:        "Login with empty app id",
			email:       gofakeit.Email(),
			password:    randomFakePassword(),
			appID:       emptyAppID,
			expectedErr: "empty app id",
		},
		{
			name:        "Login with wrong app id",
			email:       "test@test.com",
			password:    "test",
			appID:       wrongAppId,
			expectedErr: "invalid app id",
		},
		{
			name:        "Login with wrong password",
			email:       gofakeit.Email(),
			password:    "wrong",
			appID:       appID,
			expectedErr: "invalid email or password",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(
			tc.name, func(t *testing.T) {
				t.Parallel()

				_, err := st.AuthClient.Login(
					ctx, &ssov1.LoginRequest{
						Email:    tc.email,
						Password: tc.password,
						AppId:    tc.appID,
					},
				)
				require.Error(t, err)
				assert.ErrorContains(t, err, tc.expectedErr)
			},
		)
	}
}
